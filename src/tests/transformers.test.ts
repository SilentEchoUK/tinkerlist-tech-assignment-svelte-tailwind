import {
  firstIcon,
  firstMain,
  humidity,
  pressure,
  sunrise,
  sunset,
  temp,
  todayHigh,
  todayLow,
  windSpeed,
} from "$lib/weather-data/transformers";
import { describe, expect, it, vi } from "vitest";
import { exampleOpenWeatherCurrentResponse } from "./examples/openWeatherCurrentResponse";
import { exampleOpenWeatherHistoricalResponse } from "./examples/openWeatherHistoricalRequest";

describe("check transformers", () => {
  it("should get todayHigh", () => {
    const date = new Date(exampleOpenWeatherCurrentResponse.current.dt * 1000);
    vi.useFakeTimers();
    vi.setSystemTime(date);
    expect(todayHigh(exampleOpenWeatherCurrentResponse)).toBe(288.16);
    vi.useRealTimers();
  });

  it("should get todayLow", () => {
    const date = new Date(exampleOpenWeatherCurrentResponse.current.dt * 1000);
    vi.useFakeTimers();
    vi.setSystemTime(date);
    expect(todayLow(exampleOpenWeatherCurrentResponse)).toBe(277.55);
    vi.useRealTimers();
  });

  it("should get windSpeed", () => {
    expect(windSpeed(exampleOpenWeatherCurrentResponse.current)).toBe(4.02);
  });

  it("should get humidity", () => {
    expect(humidity(exampleOpenWeatherCurrentResponse.current)).toBe(68);
  });

  it("should get sunrise", () => {
    expect(sunrise(exampleOpenWeatherCurrentResponse.current)).toBe(1674911692);
  });

  it("should get sunset", () => {
    expect(sunset(exampleOpenWeatherCurrentResponse.current)).toBe(1674949384);
  });

  it("should get pressure", () => {
    expect(pressure(exampleOpenWeatherCurrentResponse.current)).toBe(1020);
  });

  it("should get firstIcon from current", () => {
    expect(firstIcon(exampleOpenWeatherCurrentResponse.current)).toBe("04d");
  });

  it("should get firstIcon from daily", () => {
    expect(firstIcon(exampleOpenWeatherCurrentResponse.daily[0])).toBe("10d");
  });

  it("should get firstIcon from historical", () => {
    expect(firstIcon(exampleOpenWeatherHistoricalResponse.data[0])).toBe("13n");
  });

  it("should get firstMain from current", () => {
    expect(firstMain(exampleOpenWeatherCurrentResponse.current)).toBe("Clouds");
  });

  it("should get firstMain from daily", () => {
    expect(firstMain(exampleOpenWeatherCurrentResponse.daily[0])).toBe("Rain");
  });

  it("should get firstMain from historical", () => {
    expect(firstMain(exampleOpenWeatherHistoricalResponse.data[0])).toBe(
      "Snow"
    );
  });

  it("should get temp from current", () => {
    expect(temp(exampleOpenWeatherCurrentResponse.current)).toBe(282.2);
  });

  it("should get temp from daily", () => {
    expect(temp(exampleOpenWeatherCurrentResponse.daily[0])).toBe(283.29);
  });

  it("should get temp from historical", () => {
    expect(temp(exampleOpenWeatherHistoricalResponse.data[0])).toBe(264.7);
  });
});
