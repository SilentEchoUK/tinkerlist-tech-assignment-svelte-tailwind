export const exampleOpenWeatherHistoricalResponse = {
  lat: 39.0997,
  lon: -94.5783,
  timezone: "America/Chicago",
  timezone_offset: -21600,
  data: [
    {
      dt: 1643803200,
      sunrise: 1643808242,
      sunset: 1643845198,
      temp: 264.7,
      feels_like: 257.7,
      pressure: 1023,
      humidity: 86,
      dew_point: 262.99,
      clouds: 100,
      visibility: 1207,
      wind_speed: 10.29,
      wind_deg: 10,
      wind_gust: 12.86,
      weather: [
        {
          id: 601,
          main: "Snow",
          description: "snow",
          icon: "13n",
        },
        {
          id: 701,
          main: "Mist",
          description: "mist",
          icon: "50n",
        },
      ],
      snow: {
        "1h": 1.01,
      },
    },
  ],
};
