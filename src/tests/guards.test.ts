import {
  isOpenWeatherCurrentResponse,
  isOpenWeatherHistoricalResponse,
  isOurResponse,
} from "$lib/guards";
import { describe, expect, it } from "vitest";
import { exampleOpenWeatherCurrentResponse } from "./examples/openWeatherCurrentResponse";
import { exampleOpenWeatherHistoricalResponse } from "./examples/openWeatherHistoricalRequest";

describe("check guards", () => {
  it("should check OpenWeatherCurrentResponse", () => {
    expect(
      isOpenWeatherCurrentResponse(exampleOpenWeatherCurrentResponse)
    ).toBe(true);
    expect(
      isOpenWeatherCurrentResponse(exampleOpenWeatherHistoricalResponse)
    ).toBe(false);
    expect(isOpenWeatherCurrentResponse({})).toBe(false);
    expect(isOpenWeatherCurrentResponse(null)).toBe(false);
    expect(isOpenWeatherCurrentResponse(undefined)).toBe(false);
    expect(isOpenWeatherCurrentResponse(0)).toBe(false);
    expect(isOpenWeatherCurrentResponse("")).toBe(false);
  });

  it("should check OpenWeatherRecentResponse", () => {
    expect(
      isOpenWeatherHistoricalResponse(exampleOpenWeatherCurrentResponse)
    ).toBe(false);
    expect(
      isOpenWeatherHistoricalResponse(exampleOpenWeatherHistoricalResponse)
    ).toBe(true);
    expect(isOpenWeatherHistoricalResponse({})).toBe(false);
  });

  it("should check OurResponse", () => {
    expect(isOurResponse(exampleOpenWeatherCurrentResponse)).toBe(false);
    expect(isOurResponse(exampleOpenWeatherHistoricalResponse)).toBe(false);
    expect(isOurResponse({})).toBe(false);
    expect(isOurResponse(null)).toBe(false);
    expect(isOurResponse(undefined)).toBe(false);
    expect(isOurResponse(0)).toBe(false);
    expect(isOurResponse("")).toBe(false);

    expect(
      isOurResponse({
        current: exampleOpenWeatherCurrentResponse,
        recent: exampleOpenWeatherHistoricalResponse,
      })
    ).toBe(false);

    expect(
      isOurResponse({
        current: exampleOpenWeatherCurrentResponse,
        recent: [exampleOpenWeatherHistoricalResponse],
      })
    ).toBe(true);

    expect(
      isOurResponse({
        current: exampleOpenWeatherCurrentResponse,
        recent: [
          exampleOpenWeatherHistoricalResponse,
          exampleOpenWeatherHistoricalResponse,
        ],
      })
    ).toBe(true);
  });
});
