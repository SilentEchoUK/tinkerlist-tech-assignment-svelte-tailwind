import { timedMemoize } from "$lib/utils";
import { describe, expect, it, vi } from "vitest";

describe("check utils", () => {
  it("should check timedMemoize", () => {
    const testFn = (a: any) => a * 2;
    vi.useFakeTimers();

    const date = new Date(2000, 0, 1, 1, 1);
    const twoMinutesLater = new Date(2000, 0, 1, 1, 3);
    const fourMinutesLater = new Date(2000, 0, 1, 1, 5);

    vi.setSystemTime(date);
    const mockFn = vi.fn(testFn);

    const memoA = timedMemoize(1, mockFn);

    expect(memoA(1)).toBe(2);

    expect(memoA(1)).toBe(2);

    expect(mockFn).toHaveBeenCalledOnce();

    mockFn.mockClear();

    vi.setSystemTime(twoMinutesLater);

    expect(memoA(1)).toBe(2);

    vi.setSystemTime(fourMinutesLater);
    expect(memoA(1)).toBe(2);

    expect(mockFn).toHaveBeenCalledTimes(2);

    vi.useRealTimers();
  });
});
