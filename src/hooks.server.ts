import type { HandleServerError } from "@sveltejs/kit";

export const handleError = (({ error, event }) => {
  console.error(error);
  return {
    status: 500,
    message: JSON.stringify({
      error,
    }),
  };
}) satisfies HandleServerError;
