import { checkResponse, parseResponse } from "$lib/fetch";
import {
  isOpenWeatherCurrentResponse,
  isOpenWeatherHistoricalResponse,
} from "$lib/guards";

import { error, json } from "@sveltejs/kit";
import { config } from "dotenv";

import { array as A, function as f, task as T, taskEither as TE } from "fp-ts";

import type { RequestHandler } from "./$types";

config();
const { OPEN_WEATHER_MAP_API_KEY } = process.env;

const unixDay = 86400;

export const getLastFiveDaysUnixDTs = () => [
  new Date().setHours(0, 0, 1, 0) / 1000 - unixDay * 5,
  new Date().setHours(0, 0, 1, 0) / 1000 - unixDay * 4,
  new Date().setHours(0, 0, 1, 0) / 1000 - unixDay * 3,
  new Date().setHours(0, 0, 1, 0) / 1000 - unixDay * 2,
  new Date().setHours(0, 0, 1, 0) / 1000 - unixDay,
];

export const POST = (async ({ request, fetch }) => {
  const fetchToTE = (
    url: string
  ): TE.TaskEither<{ message: string; error: unknown }, Response> =>
    TE.tryCatch(
      () => fetch(url),
      (error) => ({
        message: `Unable to fetch weather: ${url}`,
        error,
      })
    );

  const fetchCurrent = (lat: number, lon: number) =>
    f.pipe(
      `https://api.openweathermap.org/data/3.0/onecall?lat=${lat}&lon=${lon}&appid=${OPEN_WEATHER_MAP_API_KEY}`,
      fetchToTE,
      parseResponse,
      checkResponse(isOpenWeatherCurrentResponse),
      TE.fold((e) => {
        console.log("Unable to fetch current weather", e);
        //explicitly throw if current weather fails to fetch
        throw error(500, "Unable to fetch from OpenWeather");
      }, T.of),
      (task) => task()
    );

  const fetchRecent = (lat: number, lon: number) =>
    f.pipe(
      getLastFiveDaysUnixDTs(),
      A.map(
        (unixDT) =>
          `https://api.openweathermap.org/data/3.0/onecall/timemachine?lat=${lat}&lon=${lon}&dt=${unixDT}&appid=${OPEN_WEATHER_MAP_API_KEY}`
      ),
      A.map(fetchToTE),
      A.map(parseResponse),
      A.map(checkResponse(isOpenWeatherHistoricalResponse)),
      A.map(
        TE.fold((e) => {
          console.log("Unable to fetch recent weather", e);
          //return a never task if a day from the recent weather fails to fetch
          return T.never;
        }, T.of)
      ),
      T.sequenceArray,
      (task) => task()
    );

  const { lat, lon } = await request.json();

  if (typeof parseInt(lat) !== "number" || typeof parseInt(lon) !== "number") {
    console.log("invalid parameters", "lat", lat, "lon", lon);
    throw error(400, "Missing lat or lon");
  }
  console.log("lat", lat, "lon", lon);
  try {
    return json({
      current: await fetchCurrent(lat, lon),
      recent: await fetchRecent(lat, lon),
    });
  } catch (e) {
    console.log(e);
    throw error(500, "Unable to fetch from OpenWeather");
  }
}) satisfies RequestHandler;
