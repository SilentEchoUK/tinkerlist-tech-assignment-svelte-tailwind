import { function as f } from "fp-ts";
import db from "localforage";
import { get, writable, type Writable } from "svelte/store";

import { browser } from "$app/environment";
import { option as O } from "fp-ts";

type PersistentStore<T> = Writable<T> & {
  load: () => Promise<T>;
  reset: () => T;
  value: O.Option<T>;
};

export const persistent = <T>(
  key: string,
  defaultValue: T
): PersistentStore<T> => {
  let initialized = false;

  const { set, subscribe, update } = writable(defaultValue);

  if (!browser)
    return {
      set,
      subscribe,
      load: async () => defaultValue,
      reset: () => defaultValue,
      value: O.some(get({ subscribe })),
      update,
    };

  const prefix = "__svelte-weather-";
  const load = async (): Promise<T> => {
    try {
      const value = (await db.getItem(prefix + key)) as T;
      if (value || typeof value === "boolean") {
        console.info(`Loaded ${key} from persistent store`, value);
        set(value);
        initialized = true;
        return value;
      } else {
        console.warn(
          `No value found for key ${key}, using default value`,
          defaultValue
        );
        set(defaultValue);
        initialized = true;
        return defaultValue;
      }
    } catch (err) {
      console.error("Failed to load persistent store", {}, err);
      set(defaultValue);
      return defaultValue;
    }
  };

  const save = (value: T) => {
    if (initialized) {
      db.setItem(prefix + key, value)
        .then(() => {
          set(value);
        })
        .catch((err) => {
          console.error("Failed to save value", err);
        });
    }
  };

  const updateSavedValue = (fn: (value: T) => T) => {
    if (initialized) {
      f.pipe({ subscribe }, get, fn, save);
    }
  };

  load();

  return {
    set: save,
    update: updateSavedValue,
    subscribe,
    load,
    reset: () => {
      save(defaultValue);
      return defaultValue;
    },
    get value() {
      if (initialized) return O.some(get({ subscribe }));
      return O.none;
    },
  };
};
