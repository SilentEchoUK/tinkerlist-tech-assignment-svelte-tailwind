import { function as f, taskEither as TE } from "fp-ts";

import { isOurResponse } from "./guards";
import type { City } from "./types";
import { timedMemoize } from "./utils";

export const fetchToTE = (
  url: string
): TE.TaskEither<{ message: string; error: unknown }, Response> =>
  TE.tryCatch(
    () => fetch(url),
    (error) => ({
      message: `Unable to fetch weather: ${url}`,
      error,
    })
  );

export const parseResponse = TE.chain((response: Response) =>
  TE.tryCatch(
    () => response.json(),
    (error) => ({ message: "Error parsing weather data", error })
  )
);

export const checkResponse = <T>(guard: (data: any) => data is T) =>
  TE.chainW((data: any) => {
    if (guard(data)) {
      return TE.right(data);
    } else {
      return TE.left({
        message: `Weather data is invalid.`,
        data: data,
        error: new Error("Invalid data"),
      });
    }
  });

export const unMemoedFetchCityWeather = (city: City) =>
  f.pipe(
    TE.tryCatch(
      () =>
        fetch(".", {
          method: "POST",
          body: JSON.stringify({
            lat: city.lat,
            lon: city.lng,
          }),
        }),
      (error) => ({ message: "Error fetching weather data", error })
    ),
    parseResponse,
    checkResponse(isOurResponse)
  )();

export const fetchCityWeather = timedMemoize(5, unMemoedFetchCityWeather);
