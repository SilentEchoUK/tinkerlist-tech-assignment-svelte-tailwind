import { function as f, map as FMap, option as O, string as Str } from "fp-ts";
import type { City } from "./types";

type CachedResult = {
  expires: number;
  value: any;
};

const map = new Map<string, CachedResult>();

export const timedMemoize =
  <T extends any[], U>(minutes: number, fn: (...args: T) => U) =>
  (...args: T): U => {
    const argsString = JSON.stringify(args) + fn.name;

    console.log("memoize", argsString);
    const result = f.pipe(
      map,
      FMap.lookup(Str.Eq)(argsString),
      O.chain((cachedResult) => {
        if (cachedResult.expires > Date.now()) {
          return O.some(cachedResult.value);
        } else {
          return O.none;
        }
      }),
      O.getOrElse(() => fn(...args))
    );

    map.set(argsString, {
      value: result,
      expires: Date.now() + 1e3 * 60 * minutes,
    });

    console.log(map, result);

    return result;
  };

export const kToF = (k: number) => (k - 273.15) * 1.8 + 32;
export const kToC = (k: number) => k - 273.15;

export const printCity = (city: City) => `${city.name}, ${city.country}`;

export const printNumericDate = (unixDT: number) =>
  new Date(unixDT * 1e3).toLocaleString(undefined, {
    day: "2-digit",
    month: "2-digit",
  });

export const printDay = (unixDT: number) =>
  new Date(unixDT * 1e3).toLocaleString(undefined, {
    weekday: "long",
  });
