export interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface HourlyWeatherData {
  dt: number;
  temp?: number;
  feels_like?: number;
  pressure?: number;
  humidity?: number;
  dew_point?: number;
  uvi?: number;
  clouds?: number;
  visibility?: number;
  wind_speed?: number;
  wind_deg?: number;
  wind_gust?: number;
  weather?: Weather[];
  pop?: number;
  rain?: {
    "1h": number;
  };
}

export interface Temp {
  day: number;
  min: number;
  max: number;
  night: number;
  eve: number;
  morn: number;
}

export interface Feels_like {
  day: number;
  night: number;
  eve: number;
  morn: number;
}

export interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface BaseWeatherData {
  dt: number;
  sunrise?: number;
  sunset?: number;
  moonrise?: number;
  moonset?: number;
  moon_phase?: number;
  pressure?: number;
  humidity?: number;
  dew_point?: number;
  wind_speed?: number;
  wind_deg?: number;
  wind_gust?: number;

  clouds?: number;
  pop?: number;
  uvi?: number;
  rain?: number;
}

export type CurrentWeatherData = BaseWeatherData & {
  weather: Weather[];
  temp: number;
  wind_speed: number;
  humidity: number;
  sunrise: number;
  sunset: number;
  pressure: number;
};

export type DailyWeatherData = BaseWeatherData & {
  weather: Weather[];
  temp: Temp;
  feels_like: Feels_like;
};

export type HistoricalWeatherData = BaseWeatherData & {
  temp: number;
  weather: Weather[];
};

export type WeatherData =
  | CurrentWeatherData
  | DailyWeatherData
  | HistoricalWeatherData;

export interface Alert {
  sender_name: string;
  event: string;
  start: number;
  end: number;
  description: string;
  tags: any[];
}

export interface OpenWeatherCurrentResponse {
  lat: number;
  lon: number;
  timezone: string;
  timezone_offset: number;
  current: CurrentWeatherData;
  minutely?: {
    dt: number;
    precipitation: number;
  }[];
  hourly: HourlyWeatherData[];
  daily: DailyWeatherData[];
  alerts?: Alert[];
}

export interface OpenWeatherHistoricalResponse {
  lat: number;
  lon: number;
  timezone: string;
  timezone_offset: number;
  data: HistoricalWeatherData[];
}

export interface City {
  country: string;
  name: string;
  lat: string;
  lng: string;
}

export interface AppState {
  currentWeatherResponse: OpenWeatherCurrentResponse;
  historicalWeatherResponse: OpenWeatherHistoricalResponse[];
  location: City;
}
