import type {
  CurrentWeatherData,
  OpenWeatherCurrentResponse,
  WeatherData,
} from "$lib/types";

import { array as A, function as f, option as O } from "fp-ts";

const isToday = (unixDT: number) => {
  const today = new Date();
  const dateToCheck = new Date(unixDT * 1000);

  return (
    today.getDate() === dateToCheck.getDate() &&
    today.getMonth() === dateToCheck.getMonth() &&
    today.getFullYear() === dateToCheck.getFullYear()
  );
};

export const todayHigh = (data: OpenWeatherCurrentResponse) =>
  f.pipe(
    data.daily,
    A.findFirst((day) => isToday(day.dt)),
    O.chain((day) => O.fromNullable(day.temp.max)),
    O.getOrElse(() => data.current.temp)
  );

export const todayLow = (data: OpenWeatherCurrentResponse) =>
  f.pipe(
    data.daily,
    A.findFirst((day) => isToday(day.dt)),
    O.chain((day) => O.fromNullable(day.temp.min)),
    O.getOrElse(() => data.current.temp)
  );

export const windSpeed = (data: CurrentWeatherData) => data.wind_speed;
export const humidity = (data: CurrentWeatherData) => data.humidity;
export const sunrise = (data: CurrentWeatherData) => data.sunrise;
export const sunset = (data: CurrentWeatherData) => data.sunset;
export const pressure = (data: CurrentWeatherData) => data.pressure;

export const firstIcon = (data: WeatherData) => data.weather[0].icon;

export const firstMain = (data: WeatherData) => data.weather[0].main;

export const temp = (data: WeatherData) =>
  typeof data.temp === "object" ? data.temp.day : data.temp;
