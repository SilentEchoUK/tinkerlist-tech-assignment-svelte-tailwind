import type {
  OpenWeatherCurrentResponse,
  OpenWeatherHistoricalResponse,
} from "./types";

import { array as A } from "fp-ts";

export const isOpenWeatherCurrentResponse = (
  data: any
): data is OpenWeatherCurrentResponse => {
  if (typeof data !== "object") return false;
  if (!data) return false;
  return (
    "lat" in data &&
    "lon" in data &&
    "timezone" in data &&
    "timezone_offset" in data &&
    "current" in data &&
    "daily" in data &&
    "temp" in data.current &&
    "weather" in data.current &&
    "wind_speed" in data.current &&
    "humidity" in data.current &&
    "sunrise" in data.current &&
    "sunset" in data.current &&
    "pressure" in data.current &&
    Array.isArray(data.daily) &&
    Array.isArray(data.current.weather) &&
    data.current.weather.length > 0
  );
};

export const isOpenWeatherHistoricalResponse = (
  data: any
): data is OpenWeatherHistoricalResponse => {
  if (typeof data !== "object") return false;
  if (!data) return false;
  return (
    "lat" in data &&
    "lon" in data &&
    "timezone" in data &&
    "timezone_offset" in data &&
    "data" in data &&
    Array.isArray(data.data) &&
    "temp" in data.data[0] &&
    "weather" in data.data[0] &&
    Array.isArray(data.data[0].weather) &&
    data.data.length > 0 &&
    data.data[0].weather.length > 0
  );
};

export const isOurResponse = (
  data: any
): data is {
  current: OpenWeatherCurrentResponse;
  recent: OpenWeatherHistoricalResponse[];
} => {
  if (typeof data !== "object") return false;
  if (!data) return false;
  if (!Array.isArray(data.recent)) return false;

  return "current" in data && "recent" in data
    ? isOpenWeatherCurrentResponse(data.current) &&
        A.every(isOpenWeatherHistoricalResponse)(data.recent)
    : false;
};
