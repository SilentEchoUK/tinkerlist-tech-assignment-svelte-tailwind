import { persistent } from "./persistentStore";
import type { City } from "./types";

export const selectedCity = persistent<City | null>("selectedCity", null);
