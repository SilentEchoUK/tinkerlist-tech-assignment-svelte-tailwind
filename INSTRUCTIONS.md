# Technical Front End Developer Assignment for Svelte and Tailwind CSS
If you made it all the way here, congratulations! A new challenge has arrived though. :muscle:

Time for a technical assignment! You have one week to complete the assignment. Please let us know if there are any unexpected delays or reasons why you can’t take the test at this time. For any further questions, contact us via you HR contact at our company.

## Instructions
+ Clone this repo
+ Complete this exercise and submit either a zip of the solution or a link to a new repo
+ Please target the latest stable release of Svelte and Tailwind CSS
+ Use the [OpenWeatherMap API](https://openweathermap.org/api) for weather data
+ The [one Call API 1.0](https://openweathermap.org/api/one-call-api) should help you retrieve all you need

## Requirements
+ Solution should be responsive
+ There should be an input where the user can write the location (city, for example) they want to get info for
+ Show the weather info currently for the input location:
  + Location (ie. Brussels, Belgium)
  + Current weather description (ie. raining)
  + Current temperature
  + Today’s high temperature
  + Today’s low temperature
+ Add extra info about the current conditions on a hidden component the user can toggle the visibility:
    + Wind Speed
    + Humidity
    + Pressure
    + Sunrise/Sunset Time
+ Show basic weather info for the next 7 days on that location
+ Show basic weather info for the last 5 days on that location
+ If there’s a need to make a big number of requests, make them concurrent
+ Lazy-load the weather info

## Bonus Round:
_Was this too easy?_
  + Pre-fill the input field with the user current location
  + Deliver the solution hosted on your favourite cloud service with any appropriate changes you’d feel are relevant for a hosted solution
  + Show the location on a map
  + Auto complete the location input as the user types
  + Surprise us :)


## Review Process

Please include a README file listing how to make your solution run locally and a general description of your delivered solution and process. During the review process we'll be looking at:
+ Compliance with the requirements
+ Actual usability of the solution
+ Responsiveness
+ Git History
+ Solution structure
+ Maintainability and consistency
+ Tests (if any)
