# Technical Front End Developer Assignment for Svelte and Tailwind CSS

## Eoin Bathurst - Submission

### Quick Start

1. Clone the repo
2. Add an `.env` file to the repo root with a key for Open Weather Map's One Call API 3.0: `OPEN_WEATHER_MAP_API_KEY=key`
3. Run `npm i` then `npm start` to install dependencies, build for Node, and start the production server on `0.0.0.0:3000`

### Evaluation

I always find it challenging to balance the amount of polish and best practices to display against demonstrating a realistic speed of development against a specification to an agreed deadline. To hedge my bets on that front, I like to include a brief summary of a few of the items I cut for time and why:

- Form Actions: The hot new thing but there was a CSS interaction I didn't understand resulting in a FOUC before the enhance action finished, so I stuck to what I know best rather than pour time into that issue.
- Bundle Size: The `cities.json` package is great for development, not great for serving to clients. In a real project, I would host the data on the server and serve it lazily to the client or compress the data to serve the full dataset once and persist it locally.
- Load function: When revisiting the site, the data for the last selected city is loaded. Normally, this would be handled in the load function for the page but the way I implemented the timed cache solution for the data fetches results in a lot of boilerplate if load's fetch function is used. A refactoring of the cache system to supply the fetch function on call could be a solution for this would require a less generic implementation.
- History: I was hoping to find time for this since it probably would have solved the load function refactoring issue above. An implementation for this would use a route parameter that is an id in a cities dataset. This allows natural browser history to be used while a store would persist a unique set of city ids sorted by recently. Had this approach been in my mind initially, I would have started with it but by the time it occurred to me; it was too late to pivot to it.
- End to End tests: My experience with mocking APIs for end to end tests is lacking so I chose to focus on the other aspects of the project to showcase what I can already do.
